create database if not exists anexus;

use anexus;

create table if not exists Users(
	id int not null unique auto_increment primary Key,
	nome varchar(200) not null,
	email varchar(300) not null unique,
	senha varchar(65) not null,
        points_invite int
);

create table if not exists Invites(
	invite_id int not null unique auto_increment primary key,
        email_invite varchar(300) not null,
	email_invited varchar(300) not null,
        constraint fk_user_invite foreign key (email_invite) references Users(email)
);
