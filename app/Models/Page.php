<?php

class Page
{

    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getUsersInvites($email)
    {

        $names_inviteds = [];
        $this->db->query("SELECT email_invited FROM Invites WHERE email_invite = ?");
        $this->db->bind(1, $email);
        $results = $this->db->results();

        foreach ($results as $result) {
            $this->db->query("SELECT nome FROM Users WHERE email = ?");
            $this->db->bind(1, $result['email_invited']);
            $name = $this->db->result();
            array_push($names_inviteds, $name);
        }

        return $names_inviteds;
    }
}
