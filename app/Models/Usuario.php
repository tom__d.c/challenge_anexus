<?php 

class Usuario {

    private $db;

    public function __construct(){
        $this->db = new Database();   
    }

    public function checkEmail($email){

        $this->db->query("SELECT email FROM Users WHERE email = ?");
        $this->db->bind(1, $email);
        
        if($this->db->result()){
            return true;
        } else {
            return false;
        }

    }

    public function storage($dados){

        $this->db->query("INSERT INTO Users(nome, email, senha) VALUES (?,?,?)");

        $this->db->bind(1, $dados['name']);
        $this->db->bind(2, $dados['email']);
        $this->db->bind(3, $dados['password']);

        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }

    public function checkLogin($email, $password){

        $this->db->query('SELECT * FROM Users WHERE email = ?');
        $this->db->bind(1, $email);

        if($this->db->result()){
            $result = $this->db->result();
            if(password_verify($password, $result['senha'])){
                return $result;
            } else {
                return false;
            }
        } else {    
            return false;
        }
    }

    public function showUserById($id){

        $this->db->query('SELECT * FROM Users WHERE id = ?');
        $this->db->bind(1, $id);
        
        return $this->db->result();
    }


    public function getCountInvites($email){
        $this->db->query("SELECT COUNT(email_invite) FROM Invites WHERE email_invite = ?");
        $this->db->bind(1, $email);
        $result = $this->db->result();
        return intval($result['COUNT(email_invite)']);
    }
    
    public function getEmailById($id){
        $this->db->query("SELECT email FROM Users WHERE id = ?");
        $this->db->bind(1, $id);
        $result = $this->db->result();
        return $result['email'];
    }

    public function storageInvite($id, $email){
        $this->db->query("SELECT email FROM Users WHERE id = ?");
        $this->db->bind(1, $id);
        $result = $this->db->result();
        if($result['email']){
            
            $this->db->query("INSERT INTO Invites(email_invite, email_invited) VALUES (?,?)");
            $this->db->bind(1, $result['email']);
            $this->db->bind(2, $email);
            $this->db->execute();

            $this->db->query("SELECT COUNT(email_invite) FROM Invites WHERE email_invite = ?");
            $this->db->bind(1, $result['email']);
            $value = $this->db->result();

            if($value === NULL){
                $this->db->query("UPDATE Users SET points_invite=? WHERE email = ?");
                $this->db->bind(1, 1);
                $this->db->bind(2, $result['email']);
                $this->db->execute();
            }
            
            $value = intval($value['COUNT(email_invite)']);
            $this->db->query("UPDATE Users SET points_invite=? WHERE email = ?");
            $this->db->bind(1, $value++);
            $this->db->bind(2, $result['email']);
            $this->db->execute();
            
        }
    }

}

?>