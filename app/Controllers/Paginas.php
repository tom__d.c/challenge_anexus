<?php

class Paginas extends Controller
{


    public function __construct()
    {
        $this->pageModel = $this->model('Page');
    }

    public function home()
    {
        $this->view('paginas/home');
    }

    public function nice()
    {
        $this->view('paginas/nice');
    }

    public function index()
    {

        $dados = [];
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $results = $this->pageModel->getUsersInvites($_SESSION['email']);
            foreach ($results as $result) {
                array_push($dados, $result);
            }
        }


        $this->view('paginas/index', $dados);
    }

    public function welcome()
    {
        $this->view('paginas/welcome');
    }
}
