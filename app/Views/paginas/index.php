<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="index.css">
    <title>Anexus</title>
    <style>
        .section {
            width: 100;
            float: left;
        }

        .invite {
            margin: 223px 0 223px 0;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            text-align: center;
        }

        table th {
            padding: 20px;
            background-color: #ededed;
            border-radius: 6px;
            width: 300px;
            font-size: 22px;
        }
        table td {
            padding: 20px;
            border-radius: 6px;
            width: 300px;
            font-size: 20px;
        }

        #title-tree{
            font-size: 40px;
        }

    </style>
</head>

<body>
    <div class="section">
        <div class="invite">

        <h1 id="title-tree">Essa é a árvore binária de convidados</h1><br><br>
        <h2>Depois de convidar <b>duas</b> pessoas você <br>não pode mais convidar, você ganha<br> pontos a cada convidado.</h2><br><br><br><br><br><br><br><br>

        <table>
                <thead>
                    <tr>
                        <th id="top-user">
                            Sua conta
                        </th>
                    </tr>
                </thead>
            </table>

            <table>
                <thead>
                    <tr>
                        <th>Primeira Pessoa que você convidou</th>
                        <td><?php echo ucfirst($_SESSION['name']); ?></td>
                        <th>Segunda Pessoa que você convidou</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td data-label="first_user_invited"><?= isset($dados) ? $dados[0]['nome'] : '' ?></td>
                        <td></td>
                        <td data-label="second_user_invited"><?= isset($dados) ? $dados[1]['nome'] : '' ?></td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
</body>

</html>