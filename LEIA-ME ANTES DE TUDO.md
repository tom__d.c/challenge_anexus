# Challenge Anexus

### Primeiramente obrigado pela oportunidade de participar desse desafio maravilhoso, aprendi muito em pouco tempo.

### AVISO: É importante que os arquivos .htaccess estejam funcionando corretamente

### Ideia
Minha ideia foi de criar um sistema onde você pode convidar outras pessoa por link, no máximo duas pessoas. E em uma aba do menu você pode ver uma espécie de "árvore binária" de quem você convidou e se registrou no Sistema. Você deve ir até a aba "Convide" copiar o link, deslogar da sua conta, inserir o link na barra de busca e criar uma nova conta no link que você copiou. Qualquer dúvida ou problema podem entrar em contato comigo. O usuário ganha pontos por convidados.

#### Tecnologias usadas:
<p>Front-end: HTML 5 e CSS3</p>
<p>Back-end: PHP 7.4.16, PDO </p>
<p>Base de dados: MySQL Server version: 8.0.25-0ubuntu0.21.04.1 </p>

    - Nome do banco: anexus
    - Usuario banco: newuser
    - Senha banco: password
    - Host: localhost
    - Porta: 3306

<p>Arquivo inical em: public/index.php</p>
<p>Habilitar módulo mod_rewrite do Apache no Ubuntu

+ a2enmod rewrite
+ adicione o seguinte código para /etc/apache2/sites-available/default
    + AllowOverride All
+ Reinicie o Apache
+ /etc/init.d/apache2 restart</p>


